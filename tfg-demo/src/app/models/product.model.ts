/**
 * Created by jose on 6/11/16.
 */

export interface product {
  id: string,
  name: string,
  price?: number,
  categories: string[],
  image_url?: string,
  original_rating?: number
}
