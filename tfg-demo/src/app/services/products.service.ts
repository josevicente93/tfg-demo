/**
 * Created by jose on 6/11/16.
 */
import { Http, Response , RequestOptions, Headers } from '@angular/http';
import { Inject } from '@angular/core';

import { Observable } from 'rxjs/Rx';

export class ProductsService {
  constructor(@Inject(Http) private http:Http) {}

  getProducts(): Observable<Response> {
      return this.http.get('http://54.171.248.135:9999/products/10');
  }

  getProductsRecommendation(username: string): Observable<Response> {
    return this.http.get('http://52.211.154.226:8082/recommend?u_username=' + username);
  }

  getProduct(id: string): Observable<Response> {
    return this.http.get('http://54.171.248.135:9999/product/' + id);
  }

  saveSale(id: string, user_id: string): void {
    //this.http.post('http://54.171.248.135:9999/products/save_sale/', {"product_id": id, "user_id": user_id, "timestamp": Date.now()});
  }

  saveRate(id: string, user_name: string, user_rate: number, product_rate: number): void {
    console.log({"_productId": id, "_userId": user_name, "_rating": user_rate, "_productRating": product_rate, "timestamp": Date.now()});

    // this.http.post('http://54.171.248.135:9999/products/save_rate/',
    //   {"_productId": id, "_userId": user_name, "_rating": user_rate, "_productRating": product_rate, "timestamp": Date.now()});
    let headers      = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded'});
    let options       = new RequestOptions({ headers: headers });

    console.log(JSON.stringify({"_productId": id, "_userId": user_name, "_rating": user_rate, "_productRating": product_rate, "_date": Date.now()}));

    this.http.post("http://54.171.248.135:9997/ratings/save_rate/",
      JSON.stringify({"_productId": id, "_userId": user_name, "_rating": user_rate, "_productRating": product_rate, "_date": Date.now()}),
      options)
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
      .subscribe();
  }
}
