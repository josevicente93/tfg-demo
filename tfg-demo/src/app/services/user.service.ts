/**
 * Created by jose on 21/11/16.
 */
import { Http, Response } from '@angular/http';
import { Inject } from '@angular/core';
import { Observable } from 'rxjs/Rx';

export class UserService {
  constructor(@Inject(Http) private http:Http) {}

  login(userName: string): Observable<Response> {
    return this.http.get('http://54.171.248.135:9998/user/' + userName);
  }
}

export class SharedUsername {
  public static loggedUsername: string;
  public static id: string;

  constructor() {
    SharedUsername.loggedUsername = '';
    SharedUsername.id = '';
  }
}
