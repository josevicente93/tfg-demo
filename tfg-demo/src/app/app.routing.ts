/**
 * Created by jose on 15/11/16.
 */

import { RouterModule } from '@angular/router';

import { ProductComponent } from './components/product.component';
import { ProductsListComponent } from './components/productsList.component';

const appRoutes = [
  { path: 'products', component: ProductsListComponent },
  { path: 'product/:id', component: ProductComponent },
  { path: '', redirectTo: 'products', pathMatch: 'full'}
];

export const routing = RouterModule.forRoot(appRoutes);
