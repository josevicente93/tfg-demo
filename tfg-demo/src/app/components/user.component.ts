/**
 * Created by jose on 21/11/16.
 */

import { Component, Inject, EventEmitter, Output } from '@angular/core';
import { UserService } from '../services/user.service';
import { SharedUsername } from '../services/user.service';

@Component({
  selector: 'user',
  templateUrl: './templates/user.component.html',
  providers: [ UserService ]
})
export class UserComponent {
  private username: string;
  private loggedIn: boolean;
  private userId: string;
  private password: string;

  @Output()
  private onLoggedIn = new EventEmitter();

  constructor(@Inject(UserService) private userService: UserService) { }

  ngOnInit() {
    this.loggedIn = false;
    this.username = '';
    this.userId = '';
  }

  onSubmit() {
    this.password = '';

    this.userService.login(this.username).subscribe(
      (data) => {
        let json_response = data.json();

        if (json_response.length > 0) {
          this.username = json_response[0].login.username;
          this.userId = json_response[0]._id;
          this.loggedIn = true;
          SharedUsername.loggedUsername = this.username;
          SharedUsername.id = this.userId;

          this.onLoggedIn.emit({value: true});

        } else {
          this.username = '';
          this.userId = '';
          this.loggedIn = false;
          SharedUsername.loggedUsername = '';
          SharedUsername.id = '';
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  logout() {
    this.username = '';
    this.userId = '';
    this.loggedIn = false;
    SharedUsername.loggedUsername = '';
    SharedUsername.id = '';
  }

  getUsername() {
    return this.username;
  }
}
