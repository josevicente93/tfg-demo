/**
 * Created by jose on 6/11/16.
 */

import { Component, Inject, Input } from '@angular/core';
import { ProductsService } from '../services/products.service';

@Component({
    selector: 'recommendation-list',
    templateUrl: './templates/recommendationList.component.html',
    providers: [ ProductsService ],
    styles: [
      '.carousel-inner > .item > a > img { margin: 0 auto; }'
    ]
})
export class RecommendationListComponent {
    private recommendationList: any[];

    @Input() private username: string;
    @Input() private section: string;

    private carousellId: string;

    constructor(@Inject(ProductsService) private productsService: ProductsService) { }

    ngOnInit() {
      this.carousellId = Math.random().toString(36).substring(7);
      this.recommendationList = [{}];
      this.fetchRecommendationProducts();
    }

    fetchRecommendationProducts() {
      if (this.username != '') {
        this.productsService.getProductsRecommendation(this.username).subscribe(
          (data) => {
            let json_response = data.json();

            //let recommendationMock = '{"_categories": [{"_productList": [{"_imageURL": "https://images-na.ssl-images-amazon.com/images/I/41q5Le2g9HL._SX300_QL70_.jpg", "_avgRating": 4.841207256504345, "_mainCategory": "Electronics", "_categories": ["Electronics", "Portable Audio & Video", "MP3 & MP4 Players & Accessories", "MP3 & MP4 Players"], "_name": "Apple iPod nano 16GB Silver (7th Generation) with Generic Earpods and USB Data Cable Packaged In Non Retail White Box", "_noPurchases": 0, "_id": "B015M78NX6"}, {"_imageURL": "https://images-na.ssl-images-amazon.com/images/I/41RSPn9FWcL._SY300_QL70_.jpg", "_avgRating": 4.829719058374586, "_mainCategory": "Electronics", "_categories": ["Electronics", "Portable Audio & Video", "CB & Two-Way Radios", "Two-Way Radios"], "_name": "Midland LXT600VP3 36-Channel GMRS with 30-Mile Range, NOAA Weather Alert, Rechargeable Batteries and Charger", "_noPurchases": 0, "_id": "B007B5ZR4G"}, {"_imageURL": "https://images-na.ssl-images-amazon.com/images/I/51qmvgnzieL._SX300_QL70_.jpg", "_avgRating": 4.825433797512558, "_mainCategory": "Electronics", "_categories": ["Electronics", "Portable Audio & Video", "Boomboxes"], "_name": "Sony Portable Full Range Stereo Boombox Sound System with MP3 CD Player, AM/FM Radio, 30 Presets, USB Input, Headphone & AUX Jack - Bonus DB Sonic CD Head Cleaner", "_noPurchases": 0, "_id": "B011CUSX3C"}, {"_imageURL": "https://images-na.ssl-images-amazon.com/images/I/51fktbu0vlL._SY300_QL70_.jpg", "_avgRating": 4.801306776070605, "_mainCategory": "Electronics", "_categories": ["Electronics", "Portable Audio & Video", "CB & Two-Way Radios", "Two-Way Radios"], "_name": "Motorola MH230TPR Rechargeable Two Way Radio 3 Pack, FRS/GMRS", "_noPurchases": 0, "_id": "B0058W1I38"}, {"_imageURL": "https://images-na.ssl-images-amazon.com/images/I/31oqUHv2jjL._SY300_QL70_.jpg", "_avgRating": 4.793293548211396, "_mainCategory": "Electronics", "_categories": ["Electronics", "Portable Audio & Video", "MP3 & MP4 Players & Accessories", "MP3 & MP4 Player Accessories", "Portable Speakers & Docks", "Audio Docks"], "_name": "Aibocn Apple MFi Certified 30 Pin Sync and Charge Dock Cable for iPhone 4 4S / iPad 1 2 3 / iPod Nano / iPod Touch - White", "_noPurchases": 0, "_id": "B009N72TAA"}], "_categoryName": "Portable Audio & Video"}, {"_productList": [{"_imageURL": "https://images-na.ssl-images-amazon.com/images/I/31%2B68F0UFIL._SY300_QL70_.jpg", "_avgRating": 4.987828102255732, "_mainCategory": "Office Products", "_categories": ["Office Products", "Office Electronics", "Printers & Accessories", "Printer Parts & Accessories", "Printer Developers"], "_name": "Pantone Pastel/Neons Chip Book Gb1504", "_noPurchases": 0, "_id": "159065319X"}, {"_imageURL": "https://images-na.ssl-images-amazon.com/images/I/41fdczAgr7L._SX300_QL70_.jpg", "_avgRating": 4.981785085781575, "_mainCategory": "Office Products", "_categories": ["Office Products", "Office Electronics", "Printers & Accessories", "Printer Parts & Accessories", "Printer Cutters"], "_name": "Rotatrim RC RCMON13 13-inch Rotatrim Monorail", "_noPurchases": 0, "_id": "B001GAQJY4"}, {"_imageURL": "https://images-na.ssl-images-amazon.com/images/I/3153br2RBsL._SY300_QL70_.jpg", "_avgRating": 4.978625119034668, "_mainCategory": "Office Products", "_categories": ["Office Products", "Office Electronics", "Printers & Accessories", "Printer Parts & Accessories", "Printer Photoconductors"], "_name": "C734X24G Photoconductor Kit, 20000 Page Yield, 4/Pack", "_noPurchases": 0, "_id": "B00HV3P9XC"}, {"_imageURL": "https://images-na.ssl-images-amazon.com/images/I/416G7QFWXNL._SY300_QL70_.jpg", "_avgRating": 4.967213648788265, "_mainCategory": "Office Products", "_categories": ["Office Products", "Office Electronics", "Fax Machines"], "_name": "Brother FAX-575 Personal Fax, Phone, and Copier", "_noPurchases": 0, "_id": "B0007KI6PE"}, {"_imageURL": "https://images-na.ssl-images-amazon.com/images/I/41p%2B-H5G3nL._SX300_QL70_.jpg", "_avgRating": 4.95524059205367, "_mainCategory": "Office Products", "_categories": ["Office Products", "Office Electronics", "Printers & Accessories", "Printer Parts & Accessories", "Printer Photoconductors"], "_name": "Photoconductor Unit", "_noPurchases": 0, "_id": "B002PINXWW"}], "_categoryName": "Office Electronics"}, {"_productList": [{"_imageURL": "https://images-na.ssl-images-amazon.com/images/I/61oW6FrYOCL._SY300_QL70_.jpg", "_avgRating": 1.1983476266591064, "_mainCategory": "Collectibles & Fine Art", "_categories": ["Collectibles & Fine Art", "Sports"], "_name": "Kansas City Royals 2015 World Series Champions Team Composite Photo", "_noPurchases": 0, "_id": "B017HHF36O"}], "_categoryName": "Collectibles & Fine Art"}]}';
            //let json_response = JSON.parse(recommendationMock);

            for (let categorie of json_response._categories) {
              if (categorie._categoryName == this.section) {
                this.recommendationList = categorie._productList;
              }
            }

          },
          (error) => {
            console.log(error);
          }
        );
      }
    }
}
