/**
 * Created by jose on 6/11/16.
 */

import { Component, Inject } from '@angular/core';
import { product } from '../models/product.model';
import { ProductsService } from '../services/products.service';

@Component({
    selector: 'products-list',
    templateUrl: './templates/productList.component.html',
    providers: [ ProductsService ]
})
export class ProductsListComponent {
    private products: product[];

    constructor(@Inject(ProductsService) private productsService: ProductsService) { }

    ngOnInit() {
      this.productsService.getProducts().subscribe(
        (data) => {
          this.products = [];

          for (let item of data.json()) {
            let aux = {
              id: item._id,
              name: item.name,
              price: item.price[0],
              categories: item.sections,
              image_url: item.image_url
            }

            this.products.push(aux);
          }
        },
        (error) => {
          this.products = [];
          console.log(error);
        }
      );
    }
}
