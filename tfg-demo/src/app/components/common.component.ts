/**
 * Created by jose on 7/11/16.
 *
 * This is a parent component for almost all components.
 * It controls the login and user info (basket included).
 */

import { Component } from '@angular/core';

@Component({
  selector: 'common-component',
  templateUrl: './templates/product.component.html'
})
export class CommonComponent {
  private username: string;

  constructor() { }

  ngOnInit() {
    this.username = 'greenelephant478';
  }
}
