/**
 * Created by jose on 7/11/16.
 */

import { Component, Inject } from '@angular/core';
import { product } from '../models/product.model';
import { ProductsService } from '../services/products.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedUsername } from '../services/user.service';

@Component({
  selector: 'product',
  templateUrl: './templates/product.component.html',
  providers: [ ProductsService ],
  styleUrls: ['../../assets/css/styleProduct.css']
})
export class ProductComponent {
  private product: product;
  private productRate: number;
  private showRateForm: number;
  ref: SharedUsername;

  constructor(@Inject(ProductsService) private productsService: ProductsService,
              @Inject(ActivatedRoute) private activatedRoute: ActivatedRoute,
              @Inject(Router) private router: Router) {}

  findProduct() {
    let id: string = this.activatedRoute.snapshot.params['id'];

    this.productsService.getProduct(id).subscribe(
      (data) => {
        for (let item of data.json()) {
          let aux = {
            id: item._id,
            name: item.name,
            price: item.price[0],
            categories: item.sections,
            image_url: item.image_url,
            original_rating: Number(item.rate)
          }

          this.product = aux;

          this.showRateForm = 0;

          this.productRate = 0;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
    this.ref = SharedUsername;

    this.showRateForm = 0;

    this.productRate = 0;

    this.product = {
      id: '',
      name: '',
      price: 0.0,
      categories: [],
      image_url: '',
      original_rating: 0
    };

    this.findProduct();

    this.router.events.subscribe((val) => {
      this.findProduct();
    });
  }

  buyProduct() {
    this.productsService.saveSale(this.product.id, SharedUsername.id);
    alert('¡Gracias por su compra!');
    this.showRateForm = 1;
  }

  rateProduct() {
    this.showRateForm = 2;
    this.productsService.saveRate(this.product.id, SharedUsername.loggedUsername, Number(this.productRate), this.product.original_rating);
  }
}
