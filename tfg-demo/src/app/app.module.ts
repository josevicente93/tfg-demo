import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { ProductsListComponent } from './components/productsList.component';
import { RecommendationListComponent } from './components/recommendationList.component';
import { ProductComponent } from './components/product.component';
import { UserComponent } from './components/user.component';
import { SharedUsername } from './services/user.service';

import { routing } from './app.routing';

@NgModule({
  declarations: [
    AppComponent,
    ProductsListComponent,
    RecommendationListComponent,
    ProductComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    routing
  ],
  providers: [SharedUsername],
  bootstrap: [AppComponent]
})
export class AppModule { }
