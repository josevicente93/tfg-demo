import { TfgDemoPage } from './app.po';

describe('tfg-demo App', function() {
  let page: TfgDemoPage;

  beforeEach(() => {
    page = new TfgDemoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
